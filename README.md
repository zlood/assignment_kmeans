# ASSIGNMENT

This is my implementation of kmeans in python. The commit history shows timeline of this assignment. All of the algorithm implemented in pure python, however the visualization make use of matplotlib library, so it has to be installed with `pip` first. In order to run this assignment, simply hit

`python3 kmeans.py`
