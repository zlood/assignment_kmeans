from kmeans import KMeans, Point, Centroid
import unittest
import random


class TestKMeans(unittest.TestCase):
    def test_centroid_position(self):
        # with p and c as
        p = [Point(x=random.uniform(0, 100), y=random.uniform(0, 100)) for _ in range(10)]
        c = [Centroid(x=random.uniform(200, 300), y=random.uniform(200, 300))]

        # transform with kmeans
        kmeans_test_position = KMeans(p, c)
        result, centroid = kmeans_test_position.fit_transform()

        # assert centroid position
        self.assertIsNot(centroid[0].coordinate, c[0].coordinate, 'centroid moved')

    def test_illegal_point_dimension(self):
        self.assertRaises(TypeError, Point, x=random.uniform(0, 100))


if __name__ == '__main__':
    unittest.main()
