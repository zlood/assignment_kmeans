import random
import math
import copy
import matplotlib.pyplot as plt

# set random big number as initial minimum
initial_minimum = random.uniform(300, 400)


# main function
def main():
    # initialize variables
    lower = 0
    upper = 200
    num_points = 140
    num_centroids = 3

    # generate random points
    X = generate_points(num_points, lower, upper)

    # generate centroids
    centroids = [Centroid(x=random.uniform(lower, upper), y=random.uniform(lower, upper))
                 for _ in range(num_centroids)]

    # run kmeans function
    kmeans = KMeans(X, centroids)
    result, final_centroids = kmeans.fit_transform()

    # print all points
    for i in result:
        print('point', i, 'is included in centroid:',
              i.cluster_id)

    print('\n')

    # print all centroids
    num = 0
    for i in final_centroids:
        print('centroid', num, 'final position is ', i)
        num += 1

    # visualization
    x = [i.coordinate[0] for i in result]
    y = [i.coordinate[1] for i in result]
    clusters = [i.cluster_id for i in result]
    fig = plt.figure()
    ax = fig.add_subplot(111)
    scatter = ax.scatter(x, y, c=clusters)


# generate random points
def generate_points(n, lower, upper):
    p = [Point(x=random.uniform(lower, upper), y=random.uniform(lower, upper)) for _ in range(n)]
    return p


# calculate euclidean distance
def get_distance(a, b):
    return math.sqrt(math.pow(a.coordinate[1] - b.coordinate[1], 2) +
                     math.pow(a.coordinate[0] - b.coordinate[0], 2))


# update cluter_id of points
def update_clusterid(X, centroids):
    converged = 0

    for i in range(len(X)):
        minimum_distance = initial_minimum
        current_cluster = 0

        for j in range(len(centroids)):
            distance = get_distance(X[i], centroids[j])

            if distance < minimum_distance:
                minimum_distance = distance
                current_cluster = j

        X[i].cluster_id = current_cluster

        if X[i].cluster_id != current_cluster:
            X[i].cluster_id = current_cluster
            converged = 1

    return converged


# update centroids position
def update_centroids(X, centroids):
    x_total = 0
    y_total = 0
    cluster_member_total = 0

    for i in range(len(centroids)):
        for j in range(len(X)):
            if X[i].cluster_id == i:
                x_total += X[j].coordinate[0]
                y_total += X[j].coordinate[1]
                cluster_member_total += 1

        if cluster_member_total > 0:
            centroids[i].coordinate[0] = x_total / cluster_member_total
            centroids[i].coordinate[1] = y_total / cluster_member_total
        return


# Point Class
class Point:
    def __init__(self, x, y, cluster_id=0):
        if x is None or y is None: raise Exception("illegal point")
        self.coordinate = [x, y]
        self.cluster_id = cluster_id

    def __repr__(self):
        return str(self.coordinate)


# Centroid Class
class Centroid:
    def __init__(self, x, y):
        self.coordinate = [x, y]

    def __repr__(self):
        return str(self.coordinate)


# K-Means Class
class KMeans:
    def __init__(self, X, centroids):
        self.centroids = centroids
        self.X = X

    def fit_transform(self):
        not_converge = 1
        centroids_copy = copy.deepcopy(self.centroids)
        while not_converge:
            update_centroids(self.X, centroids_copy)
            not_converge = update_clusterid(self.X, centroids_copy)
        return self.X, centroids_copy


# run main function
if __name__ == "__main__":
    main()
